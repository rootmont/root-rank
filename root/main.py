from flask import Flask
from flask_api import status
from flask import Response
from apscheduler.schedulers.background import BackgroundScheduler
import datetime
import time
import logging

from common.config import format_logger, date_interval, earliest_crypto_date
from root.root_rank import update_root_rank

logger = logging.getLogger('main')
format_logger(logger)
app = Flask(__name__)


@app.route('/')
def root():
    return 'I am a teapot.', 420


@app.route('/health',  methods=['GET'])
def health():
    if not scheduler.running or scheduler.get_jobs().__len__() == 0:
        return Response("{}", status=status.HTTP_500_INTERNAL_SERVER_ERROR, mimetype='application/json')
    return Response("{}", status=status.HTTP_200_OK, mimetype='application/json')


def backfill_root_ranks():
    dates = date_interval(earliest_crypto_date, datetime.date.today(), 1)
    for date in dates:
        update_root_rank(date)


now = datetime.datetime.utcnow()
jiggle = datetime.timedelta(seconds=15)
if __name__ == '__main__':
    scheduler = BackgroundScheduler()
    scheduler.add_job(
        func=backfill_root_ranks,
        trigger='interval',
        hours=2,
        max_instances=1,
        next_run_time=now + jiggle
    )
    scheduler.add_job(
        func=lambda:update_root_rank(datetime.datetime.today()),
        trigger='interval',
        hours=2,
        max_instances=1,
        next_run_time=now + jiggle
    )
    jobs = scheduler.get_jobs()
    logger.debug('Starting scheduler with {} jobs'.format(len(jobs)))
    scheduler.start()
    time.sleep(1)
    try:
        app.run(host='0.0.0.0', port=5000)
    except Exception as e:
        scheduler.shutdown()
