import datetime
import logging
from common.config import format_logger
from common.db.ranks import insert_rank
from common.db.timeseries import get_overall_ranks_for_date
from common.db.risk import get_risk_ranks_for_date
from common.db.github_db import to_data_tables, get_for_all_coins
from common.db.social import get_latest_social_ranks, get_social_ranks_for_date
import pandas as pd


logger = logging.getLogger('root-rank')
format_logger(logger)
# Usage = usage.UsageStats()
# Risk = risk.RiskStats()
# Social = social.SocialStats()


def update_root_rank(date: datetime.date=None):
    logger.info('Updating root rank for {}'.format(date))
    # social
    logger.info('Getting social stats')
    social_rank = get_social_ranks(target_date=date)
    if len(social_rank) == 0:
        logger.warning('no social stats found for {}'.format(date))
        # Social.update(date)
        # social_rank = get_social_ranks_for_date(target_date=date)

    # usage
    logger.info('Getting usage stats')
    usage_df = get_overall_ranks_for_date(date)
    if len(usage_df) == 0:
        logger.warning('no usage stats found for {}'.format(date))
        # Usage.update(date)
        # usage_df = get_overall_ranks_for_date(date)

    # dev
    logger.info('Getting dev stats')
    dev_dfs = to_data_tables(get_for_all_coins())
    if len(dev_dfs[0]) == 0:
        logger.warning('no dev stats found for {}'.format(date))

    # risk
    logger.info('Getting risk stats')
    risk_series = get_risk_ranks_for_date(date)
    if len(risk_series) == 0:
        logger.warning('no risk stats found for {}'.format(date))
        # Risk.update(date)
        # risk_series = get_risk_ranks_for_date(date)

    # root
    ranks = pd.concat([social_rank, usage_df['usage_rank'], dev_dfs[2], risk_series], axis='columns', sort=True)
    ranks.rename(columns={0:'social',1:'dev', 2:'risk', 'usage_rank': 'usage'}, inplace=True)
    ranks.fillna(0, inplace=True)
    ranks['root'] = ranks.mean(axis='columns')
    insert_ranks(ranks=ranks, date=date if date is not None else datetime.date.today())


def insert_ranks(ranks, date):
    for name in ranks['root'].index:
        insert_rank(name, date, ranks['root'].loc[name], ranks['risk'].loc[name], ranks['social'].loc[name], ranks['dev'].loc[name], ranks['usage'].loc[name])


def get_social_ranks(target_date=None):
    if target_date is None:
        df = get_latest_social_ranks()
    else:
        df = get_social_ranks_for_date(target_date=target_date)
    return df.mean(axis='columns')