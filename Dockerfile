FROM python:3.7

ARG dir=root
COPY ./common /common
COPY ./$dir /$dir
COPY ./requirements.txt /$dir
WORKDIR /$dir
RUN pip install -r requirements.txt

CMD ["python", "main.py"]